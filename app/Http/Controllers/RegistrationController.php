<?php

namespace App\Http\Controllers;

use App\LiqPay;
use Illuminate\Http\Request;
use Google_Client;
use Google_Service_Sheets;

class RegistrationController extends Controller {

	private function getGoogleSpreadSheetsClient() {
		$authConfigs = [
			"installed" => [
				"client_id" => "352214524645-v9sa8oculsodlm33da4ead6d541jef3f.apps.googleusercontent.com",
				"project_id" => "focus-conf-235521",
				"auth_uri" => "https://accounts.google.com/o/oauth2/auth",
				"token_uri" => "https://oauth2.googleapis.com/token",
				"auth_provider_x509_cert_url" => "https://www.googleapis.com/oauth2/v1/certs",
				"client_secret" => "Eoqk0oB5AOIqM7UPMc1xhcoj",
				"redirect_uris" => [
					"http://localhost:8000"
				]
			]
		];

		$client = new Google_Client();

		$client->setApplicationName('Google Sheets API PHP Quickstart');
		$client->setScopes(Google_Service_Sheets::SPREADSHEETS);
//		$client->setAuthConfig('credentials.json');
		$client->setAuthConfig($authConfigs);
		$client->setAccessType('offline');
		$client->setPrompt('select_account consent');

		// Load previously authorized token from a file, if it exists.
		// The file token.json stores the user's access and refresh tokens, and is
		// created automatically when the authorization flow completes for the first
		// time.
		$tokenPath = 'token.json';
		if (file_exists($tokenPath)) {
			$accessToken = json_decode(file_get_contents($tokenPath), true);
			$client->setAccessToken($accessToken);
		}

		// If there is no previous token or it's expired.
		if ($client->isAccessTokenExpired()) {
			// Refresh the token if possible, else fetch a new one.
			if ($client->getRefreshToken()) {
				$client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
			} else {
				// Request authorization from the user.
				$authUrl = $client->createAuthUrl();
//				printf("Open the following link in your browser:\n%s\n", $authUrl);
//				print 'Enter verification code: ';
//				$authCode = @trim(@fgets(STDIN));
				$authCode = '4/GAEAgFvMV8n3dIq4DHOxEpGIajSxKyeblRQAToHOqRLYJKZGiimBbtb0-6gIJXycwm_ynfXGxxGhGN8z0p7Bw-M';

//				die($authUrl);
				// Exchange authorization code for an access token.
				$accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
				$client->setAccessToken($accessToken);

				// Check to see if there was an error.
				if (array_key_exists('error', $accessToken)) {
					throw new \Exception(join(', ', $accessToken));
				}
			}
			// Save the token to a file.
			if (!file_exists(dirname($tokenPath))) {
				mkdir(dirname($tokenPath), 0700, true);
			}
			file_put_contents($tokenPath, json_encode($client->getAccessToken()));
		}
		return $client;
	}

	public function register(Request $request) {

		// Get the API client and construct the service object.
		$client = $this->getGoogleSpreadSheetsClient();

		$service = new Google_Service_Sheets($client);

		// Prints the names and majors of students in a sample spreadsheet:
		// https://docs.google.com/spreadsheets/d/1BxiMVs0XRA5nFMdKvBdBZjgmUUqptlbs74OgvE2upms/edit
		$spreadsheetId = '1ikfuZibXuakkBfURz4q5SoYxM94l2sFR7eN2vB___sY';
		$range = 'Sheet1!A2:E';



//	$response = $service->spreadsheets_values->get($spreadsheetId, $range);
//	$values = $response->getValues();
//
//	if (empty($values)) {
//		echo "No data found.\n";
//	} else {
//		echo "Name, Major:\n";
//		foreach ($values as $row) {
//			// Print columns A and E, which correspond to indices 0 and 4.
//			printf("%s, %s\n", $row[0], $row[4]);
//		}
//	}
		$recordId = 0;
		$countRecordsPath = 'count_records.txt';
		if (file_exists($countRecordsPath)) {
			$recordId = (integer)file_get_contents($countRecordsPath) + 1;
		}
		file_put_contents($countRecordsPath, $recordId);

		$updateRange = 'Sheet1!A' . ($recordId + 2) . ':K';
		$updateBody = new \Google_Service_Sheets_ValueRange([
			'range' => $updateRange,
			'majorDimension' => 'ROWS',
			'values' => [
				'values' => [
					$recordId + 1,
					date('Y-m-d H:i:s'),
					$request->input('firstName')?:'',
					$request->input('lastName')?:'',
					$request->input('secondName')?:'',
					$request->input('city')?:'',
					$request->input('church')?:'',
					$request->input('phone')?:'',
					$request->input('email')?:'',
					$request->input('date')?:'',
				]
			],
		]);
		$service->spreadsheets_values->update(
			$spreadsheetId,
			$updateRange,
			$updateBody,
			['valueInputOption' => 'USER_ENTERED']
		);

		return 'ok';
//		return $this->liqpay($recordId, $request->input('firstName'), $request->input('lastName'));
	}

	public function liqpay($orderId, $firstName, $lastName) {
		$liqpay = new LiqPay('i89141893604', 'HbErWWam51s5SN8ZvyX5227B4USwmZMpdU0Jo8Sz');

		$liqpayData = [
			'public_key'     => 'i89141893604',
			'action'         => 'pay',
			'amount'         => 100,
			'description'    => 'Регистрационный взнос',
			'currency'       => LiqPay::CURRENCY_UAH,
			'order_id'       => (string)$orderId,
			'version'        => '3',
			'language'       => 'uk',
			'result_url'     => env('APP_URL') . '/checkout/' . $orderId,
			'paytypes'       => 'card,liqpay,privat24',
			'sender_first_name' => $firstName,
			'sender_last_name' => $lastName,
		];

		if (env('APP_DEBUG')) {
			$liqpayData['sandbox'] = 1;
		}

		return $liqpay->cnb_form($liqpayData);
	}

	public function checkout(Request $request, $id) {

		$spreadsheetId = '1ikfuZibXuakkBfURz4q5SoYxM94l2sFR7eN2vB___sY';
		$liqpay = new LiqPay('i89141893604', 'HbErWWam51s5SN8ZvyX5227B4USwmZMpdU0Jo8Sz');

		$res = $liqpay->api("request", array(
			'action'        => 'status',
			'version'       => '3',
			'order_id'      => $id
		));

//		// Get the API client and construct the service object.
		$client = $this->getGoogleSpreadSheetsClient();

		$service = new Google_Service_Sheets($client);


		$updateRange = 'Sheet1!K' . ($id + 1);
		$updateBody = new \Google_Service_Sheets_ValueRange([
			'range' => $updateRange,
			'majorDimension' => 'ROWS',
			'values' => [
				'values' => [
					$res->status
				]
			],
		]);

		$service->spreadsheets_values->update(
			$spreadsheetId,
			$updateRange,
			$updateBody,
			['valueInputOption' => 'USER_ENTERED']
		);

		return 'ok';
	}

}