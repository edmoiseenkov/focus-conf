<?php

Route::get('/', function () {
    return view('index');
});

Route::get('/dogovor', function () {
    return view('dogovor');
});


Route::post('/register', 'RegistrationController@register');
Route::post('/checkout/{id}', 'RegistrationController@checkout');
